package edu.sjsu.mortgagecalculator;

public class CalculateUtil {
    public static float calculate(float principal, float rate, int years, boolean tax) {
        rate = ( rate/1200);
        int time = years * 12;
        float bottom = (float) (1-(Math.pow((1+(rate)),(-time))));
        if (rate == 0.0) {
            if (tax) {
                return (float) ((principal / time) + (.001 * principal));
            } else {
                return ((principal / years));
            }
        } else if (rate != 0.0 && tax) {
            return (float) ((principal * (rate/bottom)) + (.001 * principal));
        }
        else{
            return (float) (principal * (rate/bottom));
        }
    }
}
