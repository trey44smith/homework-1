package edu.sjsu.mortgagecalculator;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private EditText principal;
    private EditText rate;
    private TextView textView;
    private CheckBox checkBox;
    private Button button;
    private TextView seekBarValue;
    private SeekBar seekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        principal = (EditText) findViewById(R.id.principal);
        textView = (TextView) findViewById(R.id.textView);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        button = (Button) findViewById(R.id.button);
        seekBar = (SeekBar)findViewById(R.id.seekBar);
        seekBarValue = (TextView)findViewById(R.id.seekBarValue);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                // TODO Auto-generated method stub
                seekBarValue.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }
        });
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button:
                RadioButton fifteenButton = (RadioButton)
                        findViewById(R.id.radioButton);
                RadioButton twentyButton = (RadioButton)
                        findViewById(R.id.radioButton2);
                RadioButton thirtyButton = (RadioButton)
                        findViewById(R.id.radioButton3);
                if (principal.getText().length() == 0) {
                    Toast.makeText(this, "Please enter a valid number",
                            Toast.LENGTH_LONG).show();
                    return;
                }
                float inputValue = Float.parseFloat(principal.getText().toString());
                float interest = Float.parseFloat(seekBarValue.getText().toString());
                if (fifteenButton.isChecked() && !checkBox.isChecked()) {
                    textView.setText(String
                            .valueOf(CalculateUtil.calculate(inputValue, interest, 15, false)));
                    fifteenButton.setChecked(true);
                    twentyButton.setChecked(false);
                    thirtyButton.setChecked(false);
                } else if(twentyButton.isChecked() && !checkBox.isChecked()) {
                    textView.setText(String
                            .valueOf(CalculateUtil.calculate(inputValue, interest, 20, false)));
                    fifteenButton.setChecked(false);
                    twentyButton.setChecked(true);
                    thirtyButton.setChecked(false);
                }
                else if (thirtyButton.isChecked() && !checkBox.isChecked()){
                    textView.setText(String
                            .valueOf(CalculateUtil.calculate(inputValue, interest, 30, false)));
                    fifteenButton.setChecked(false);
                    twentyButton.setChecked(false);
                    thirtyButton.setChecked(true);
                }
                else if (fifteenButton.isChecked() && checkBox.isChecked()) {
                    textView.setText(String
                            .valueOf(CalculateUtil.calculate(inputValue, interest, 15, true)));
                    fifteenButton.setChecked(true);
                    twentyButton.setChecked(false);
                    thirtyButton.setChecked(false);
                } else if(twentyButton.isChecked() && checkBox.isChecked()) {
                    textView.setText(String
                            .valueOf(CalculateUtil.calculate(inputValue, interest, 20, true)));
                    fifteenButton.setChecked(false);
                    twentyButton.setChecked(true);
                    thirtyButton.setChecked(false);
                }
                else if (thirtyButton.isChecked() && checkBox.isChecked()){
                    textView.setText(String
                            .valueOf(CalculateUtil.calculate(inputValue, interest, 30, true)));
                    fifteenButton.setChecked(false);
                    twentyButton.setChecked(false);
                    thirtyButton.setChecked(true);
                }
                else{
                    break;
                }
        }
    }
}